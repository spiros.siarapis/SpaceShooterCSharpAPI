﻿using System;
using SSiarapis.SpaceShooterAPI;

// This program is only used to showcase
// how the api is supposed to work 
public static class Program
{
    public static void Main(string[] args)
    {
        #region Creating SpaceShooterAPI
        // Create a new SpaceShooterAPI class that connects to the server
        // The first parameter is the host name of the server
        // The second parameter is your game key which is used when sending a new highscore to the server
        SpaceShooterAPI api = new SpaceShooterAPI(
            "https://ssiarapis.online/spaceshooterapi",
            "");
        #endregion

        #region Getting HighScores
        // The GetHighScore function has a lot of parameters that you can play with
        // Some examples are:
        // id => the id of the highscore you want to get; every highscore has a different id
        // limit => the limit of how many highscores you want to get from the server
        // playerName => highscores which include this player name
        // gameName => highscores which include this game name
        // score => highscores with this score
        var highscores = api.GetHighScore();

        // This example gets 4 highscores with score 10
        var fourHighscoresWScore10 = api.GetHighScore(limit: 4, score: 10);
        
        // The GetHighScore function returns either null if it didn't get a response from the server
        // or an array of HighScore_GET classes
        // In the HighScore_GET there exists some variables that you can use when you want to display the highscore
        // There is:
        // ID => the id of the highscore,
        // PlayerName => the name of the player of this highscore
        // Score => the score of this highscore
        // GameName => the name of the game of this highscore

        // Check first so that you have gotten something from the server
        if (highscores != null)
        {
            // For every highscore that you get print its value
            foreach (var item in highscores)
            {
                Console.WriteLine(item.ID);
                Console.WriteLine(item.PlayerName);
                Console.WriteLine(item.Score);
                Console.WriteLine(item.GameName);
                Console.WriteLine(); // Empty line to make the console prettier
            }
        }

        // Check so that the server has sent something
        if(fourHighscoresWScore10 != null)
        {
            // For every highscore that you get print its value
            foreach (var item in fourHighscoresWScore10)
            {
                Console.WriteLine(item.ID);
                Console.WriteLine(item.PlayerName);
                Console.WriteLine(item.Score);
                Console.WriteLine(item.GameName);
                Console.WriteLine(); // Empty line to make the console prettier
            }
        }
        #endregion

        #region Getting HighScore Games
        // The GetGame function some parameters that you can change
        // These are the parameters: 
        // id => the id of the game you want to get; every game has a different id
        // limit => the limit of how many games you want to get from the server
        // name => the name of the game you want to get
        var games = api.GetGame();

        // This example gets max four games from the server
        var fourGames = api.GetGame(limit: 4);

        // The GetGame function returns either null if it didn't get a response from the server
        // or an array of Game_GET classes
        // The Game_GET class has three variables:
        // ID => The id of the game
        // GameName => The name of the game

        // Check so that the server didn't return null
        if (games != null) {
            // Print every game you got from the server
            foreach (var item in games)
            {
                Console.WriteLine(item.ID);
                Console.WriteLine(item.GameName);
                Console.WriteLine(); // Empty line to make the console prettier
            }
        }

        // Check that the server sent the results
        if (fourGames != null)
        {
            // Print the games sent by the server
            foreach (var item in fourGames)
            {
                Console.WriteLine(item.ID);
                Console.WriteLine(item.GameName);
                Console.WriteLine(); // Empty line to make the console prettier
            }
        }
        #endregion

        #region Inserting HighScores
        // When sending a highscore
        // Make sure you have set the correct game key when creating the SpaceShooterAPI class
        // If the insertion is successfull the server will return "Successfully inserted a new highscore!",
        // otherwise it will return "An error happened!"
        // If an error appears check that the gamekey is the same as when created
        HighScore_POST highscore = new HighScore_POST("PlayerName", 40);
        var res = api.InsertHighScore(highscore);

        if(res != null)
        {
            // You can do whatever you want with the result from the string
            // for example here I print it out to the console
            Console.WriteLine($"{res}");
        }
        #endregion
    }
}