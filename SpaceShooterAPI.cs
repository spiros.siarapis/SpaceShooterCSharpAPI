﻿using System.Text.Json;

namespace SSiarapis.SpaceShooterAPI
{
	public class SpaceShooterAPI
	{
		readonly HttpClient client = new HttpClient();

		readonly string hostName;  // The name of the websites api "folder"
		readonly string gameKey;   // The key of the game, used to insert a new high score into the database


		/// <summary>
		/// Constructor of SpaceShooterAPI
		/// </summary>
		/// <param name="hostName">The name of the websites api "folder"</param>
		/// <param name="gameKey">The key of your game, this is used when inserting a new high score</param>
		public SpaceShooterAPI(string hostName, string gameKey)
        {
			client = new HttpClient();
			this.hostName = hostName;
			this.gameKey = gameKey;
        }

		/// <summary>
		/// Generic GET function to use
		/// </summary>
		/// <param name="getRequest">The get request url from the hostName server</param>
		/// <returns>Returns a string from the server</returns>
		private async Task<string> GET(string getRequest)
        {
			try
            {
				var content = await client.GetStringAsync($"{hostName}/{getRequest}");

				return content;
            }
			catch(HttpRequestException e)
            {
				throw e;
            }
        }

		/// <summary>
		/// Generic POST function to use
		/// </summary>
		/// <param name="postRequest">The post request url from the hostName server</param>
		/// <param name="data">The data to send</param>
		/// <returns>Returns a string send from the server</returns>
		private async Task<string> POST(string postRequest, HttpContent data)
        {
			try
            {
				var content = await client.PostAsync($"{hostName}/{postRequest}", data);

				var result = await content.Content.ReadAsStringAsync();
				
				return result;
            }
			catch (HttpRequestException e)
            {
				throw e;
            }
        }

		/// <summary>
		/// Gets a highscore / highscores from the server
		/// </summary>
		/// <param name="limit">How many values to return</param>
		/// <param name="id">Highscore with id</param>
		/// <param name="gameId">Highscore with a game with id</param>
		/// <param name="playerName">Highscore with a player with name</param>
		/// <param name="gameName">Highscore with a game with name</param>
		/// <param name="score">Highscore with score</param>
		/// <returns>Array made of HighScore_GET classes</returns>
		public HighScore_GET[]? GetHighScore(
			int? limit = null, 
			int? id = null, 
			int? gameId = null, 
			string? playerName = null, 
			string? gameName = null, 
			int? score = null)
        {
			string args = "";
			if (limit.HasValue)
				args += $"limit={limit.Value}&";

			if (id.HasValue)
				args += $"id={id.Value}&";

			if (gameId.HasValue)
				args += $"gameId={gameId.Value}&";

			if (playerName != null)
				args += $"playername={playerName}&";

			if (gameName != null)
				args += $"gamename={gameName}";

			if (score.HasValue)
				args += $"score={score}";


			var request = GET($"get.php?{args}");
			request.Wait();

			var result = request.Result;

			var highScores = JsonSerializer.Deserialize<HighScore_GET[]>(result);

			return highScores;
		}

		/// <summary>
		/// Insert a new highscore with the gamekey provided when creating the class
		/// </summary>
		/// <param name="highScore">The highscore to send to the server</param>
		/// <returns>The result send from the server</returns>
		public string InsertHighScore(HighScore_POST highScore)
        {
			highScore.GameKey = gameKey;

			var data = new Dictionary<string, string>
			{
				{ "name", highScore.PlayerName },
				{ "score", highScore.Score.ToString() },
				{ "gameKey", highScore.GameKey }
			};

			var request = POST($"insertScore.php", new FormUrlEncodedContent(data));
			request.Wait();

			var result = request.Result;

			return result.ToString();
        }

		/// <summary>
		/// Gets a game / games from the server
		/// </summary>
		/// <param name="limit">How many values to return</param>
		/// <param name="id">Id of game to return</param>
		/// <param name="name">Name of game to return</param>
		/// <returns>Array made of Game_GET classes</returns>
		public Game_GET[]? GetGame(int? limit = null, int? id = null, int? name = null)
		{
			string args = "";
			if (limit.HasValue)
				args += $"limit={limit.Value}&";

			if (id.HasValue)
				args += $"id={id.Value}&";

			if (name.HasValue)
				args += $"gameName={name.Value}&";


			var request = GET($"getGame.php?{args}");
			request.Wait();

			var result = request.Result;

			var games = JsonSerializer.Deserialize<Game_GET[]>(result);

			return games;
		}
	}

	// Class for getting highscore information
	public class HighScore_GET
    {
		public string ID { get; private set; }
		public string PlayerName { get; private set; }

		public string Score { get; private set; }
		public string GameName { get; private set; }

		public HighScore_GET(string ID, string PlayerName, string Score, string GameName)
        {
			this.ID = ID;
			this.PlayerName = PlayerName;
			this.Score = Score;
			this.GameName = GameName;
        }
    }

	// Class for getting game information
	public class Game_GET
    {
		public string ID { get; private set; }
		public string GameName { get; private set; }

		public Game_GET(string ID, string GameName)
        {
			this.ID = ID;
			this.GameName = GameName;
        }
    }

	// Class for sending highscore information
	public class HighScore_POST
    {
		public string PlayerName { get; private set; }
		public int Score { get; private set; }
		public string GameKey { get; set; }

		public HighScore_POST(string PlayerName, int Score)
        {
			this.PlayerName = PlayerName;
			this.Score = Score;
			GameKey = "";
        }
    }
}
